# grpc-mock-server

Simplest `gRPC` mock-server.

---

## Usage

Let's imagine that we have next api specifications directory structure:
```shell
$ tree examples/specs
specs
└── api
    ├── common
    │   └── v1
    │       └── common.proto
    └── hello
        └── v1
            └── hello.proto


```

Where `hello.proto` has content:

```protobuf
syntax = "proto3";
package api.hello.v1;

import "api/common/v1/common.proto";

service HelloService {
  rpc Hello(HelloRequest) returns (HelloResponse);
}

message HelloRequest {
  string name = 1;
}

message HelloResponse {
  string greeting = 1;
  api.common.v1.Common common = 2;
}
```

and `common.proto`:

```protobuf
syntax = "proto3";
package api.common.v1;

message Common {
  string foo = 1;
  bool bar = 2;
}
```

Now we need to configure mock server and mocks definitions:

`examples/config/server.json`:

```json
{
    "protoPaths": [
        "/app/specs/api/hello/v1/hello.proto"
    ],
    "includeDirs": [
        "/app/specs"
    ],
    "services": [
        "api.hello.v1.HelloService"
    ],
    "port": 50051,
    "secure": false
}
```

`examples/config/mocks.json`:

```json
[
    {
        "service": "api.hello.v1.HelloService",
        "method": "Hello",
        "matches": [
            {
                "match": {"name": "John"},
                "reply": {"greeting": "Hello, John", "common": {"foo": "John", "bar": true}}
            },
            {
                "match": {"name": "Lisa"},
                "reply": {"greeting": "Have a nice day, Lisa", "common": {"foo": "Lisa", "bar": false}}
            }
        ]
    }
]
```

Now we should to get this structure of filed and directories:

```shell
examples/
├── config
│   ├── mocks.json
│   └── server.json
└── specs
    └── api
        ├── common
        │   └── v1
        │       └── common.proto
        └── hello
            └── v1
                └── hello.proto

```

Now we can start mock-server:
 
```bash
$ docker run -it --rm \
  -v $(pwd)/config:/app/config \
  -v $(pwd)/specs:/app/specs \
  -p 9080:50051 \
  registry.gitlab.com/is.it.real/grpc-mock-server:v0.0.2
  
gRPC server listening port 50051
Available grpc services:

* api.hello.v1.HelloService
         /Hello  
```

And it's ready to handle your requests:

```bash
$ grpcurl \
  -proto api/hello/v1/hello.proto \
  -import-path api  \
  -plaintext \
  -d '{"name":"John"}' \
  127.0.0.1:9080 api.hello.v1.HelloService/Hello

{
  "greeting": "Hello, John",
  "common": {
    "foo": "John",
    "bar": true
  }
}

```

```bash
$ grpcurl -proto hello/hello.proto \
  -plaintext \
  -d '{"name":"Lisa"}' \
  127.0.0.1:8090 api.hello.v1.HelloService/Hello

{
  "greeting": "Have a nice day, Lisa",
  "common": {
    "foo": "Lisa"
  }
}

```

```bash
$ grpcurl -proto hello/hello.proto \
  -plaintext \
  -d '{"name":"Piter"}' \
  127.0.0.1:8090 api.hello.v1.HelloService/Hello
  
ERROR:
  Code: Unknown
  Message: No matches found for request: {"name":"Piter"}
```