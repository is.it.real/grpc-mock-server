'use strict';

const grpc = require('grpc');

/**
 * GRPCError
 */
class GRPCError extends Error {
    /**
     * @param {number} status grpc.status.*
     * @param {string} message
     * @param {*} details
     */
    constructor(status, message, details) {
        super(`${message}: ${details ? JSON.stringify(details) : 'No details provided'}`);
        /** @public */
        this.code = status || grpc.status.UNKNOWN;
    }
}

module.exports = GRPCError;