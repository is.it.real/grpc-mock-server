'use strict';

const GRPCServer = require('./server');
const GRPCError = require('./error');
const grpc = require('grpc');
const matchingHandler = require('./matching-handler-factory');

module.exports = {
    GRPCServer,
    GRPCError,
    status: grpc.status,
    handlers: {
        matchingHandler
    }
};