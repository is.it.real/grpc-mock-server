FROM node:16.4-alpine as builder

RUN mkdir /build

WORKDIR /build

COPY package.json /build/package.json
COPY yarn.lock /build/yarn.lock

RUN set -x && yarn install

FROM node:16.4-alpine as runtime

WORKDIR /app

COPY --from=builder /build /app
COPY index.js /app
COPY src /app/src

CMD ["node", "index.js"]