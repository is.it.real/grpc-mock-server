'use strict';

const _ = require('lodash');
const path = require('path');
const easyGRPC = require('./src/server/index');
const matcherFactory = easyGRPC.handlers.matchingHandler;
const serverConfig = require(path.resolve(__dirname, 'config/server.json'))
const mocksConfig = require(path.resolve(__dirname, 'config/mocks.json'))

const server = new easyGRPC.GRPCServer({
    ...serverConfig,
    logger: console
});

const availableServices = {}

mocksConfig.forEach(item => {
    if (!availableServices.hasOwnProperty(item.service)) {
        availableServices[item.service] = []
    }

    availableServices[item.service].push(item.method)
    server.addHandler(item.service, item.method,  matcherFactory(item.matches))
})

try {
    server.start().then(port => {
        console.log(`gRPC server listening port ${port}`);
        console.info(`Available grpc services:\n`)
        Object.keys(availableServices).forEach(service => {
            console.info(`* ${service}`)
            availableServices[service].forEach(method => {
                console.info(`\t /${method} `)
            })
        })
    }).catch(error => {
        console.error(error)
    });

} catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
    process.exit(1);
}

process.on('SIGINT', function() {
    console.log("Caught interrupt signal");
    process.exit();
});