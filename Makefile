#!/usr/bin/make
# Makefile readme (ru): <http://linux.yaroslavl.ru/docs/prog/gnu_make_3-79_russian_manual.html>
# Makefile readme (en): <https://www.gnu.org/software/make/manual/html_node/index.html#SEC_Contents>

.PHONY : help up down restart logs
.DEFAULT_GOAL : help

# This will output the help for each task. thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## Show this help
	@printf "\033[33m%s:\033[0m\n" 'Available commands'
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[32m%-11s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

image: ## Build docker image with app
	docker build -f ./Dockerfile -t grpc-mock-server:local .
	@printf "\n   \e[30;42m %s \033[0m\n\n" 'Now you can use image like `docker run --rm grpc-mock-server:local ...`';

run: image ## Rebuild image and start mock server
	@printf "\n   \e[30;42m  %s  \033[0m\n\n" 'Mock gRPC Server ⇒ http://127.0.0.1:9080';
	docker run -it --rm -v $(PWD)/examples/config:/app/config -v $(PWD)/examples/specs:/app/specs -p 9080:50051 grpc-mock-server:local
